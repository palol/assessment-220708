<?php
// you can write to stdout for debugging purposes, e.g.
// print "this is a debug message\n";

/* Assuming start at A[0] */

function solution($A) {

    $next = $A[0];
    $iterator = 1;
    $steps = [];

    while($iterator < count($A) && $next < count($A)) {

        if (in_array($next, $steps))
            return -1;

        array_push($steps, $next);

        $next = $A[$next]+$next;
        $iterator++;
    }

    return $iterator;
}



 ?>
