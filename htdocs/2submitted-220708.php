<?php
// you can write to stdout for debugging purposes, e.g.
// print "this is a debug message\n";

class Solution {
    private $iteration;

    private $max_length = 6;

    private $solution;

    protected function formatter(int $number): int {
        $number_stringified = strval($number);

        if (strlen($number_stringified) > $this->max_length)
            $number_stringified = substr($number_stringified, -$this->max_length, $this->max_length);

        if ($number_stringified != '0')
            $number_stringified = ltrim($number_stringified, '0');

        return intval($number_stringified);
    }
    protected function fibonacci(): int {
        $index = 0;
        $this->iteration = $this->iteration -2;

        $penultimate = 1;
        $last = 1;

        while ($index < $this->iteration) {
        $next = $penultimate + $last;

        $penultimate = $last;
        $last = $next;

        $index++;
        }
        return $this->formatter($next);
    }

    public function getSolution(): int {
        return $this->solution;
    }

    public function __construct(int $N) {
        $this->iteration = $N;

        switch($this->iteration) {
            case 0:
                $this->solution = $this->formatter(0);
                break;
            case 1:
                $this->solution = $this->formatter(1);
                break;
            default:
                $this->solution = $this->fibonacci();

        }
    }
}

function solution($N) {
    $s = new Solution($N);
    return $s->getSolution();
}
